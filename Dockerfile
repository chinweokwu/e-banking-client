# Use an official Node.js runtime as the base image
FROM node:14

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code to the container
COPY . .

# Build your Vite project for production
RUN npm run build

# Expose the port your application will run on (Vite default is 3000)
EXPOSE 5173

# Start your application
CMD ["npm", "start"]