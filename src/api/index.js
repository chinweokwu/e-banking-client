import axios from 'axios';

const createGraphQLClient = () => {
  // const baseURL = 'http://localhost:3000/graphql';
  const baseURL = 'https://ebanking-api.onrender.com/graphql';

  const client = axios.create({
    baseURL,
  });

  return client;
};

const client = createGraphQLClient();

export default client;
