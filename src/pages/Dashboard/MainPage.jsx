import TransactionCard from "../../components/transactionCard";
import TransactionHistory from "../../components/transactionHistory";
import { useEffect } from "react";
import Loading from "../../components/loading";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch  } from "react-redux";
import { getUpdatedBalance } from "../../features/auth/authSlice";
const MainPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const balance = useSelector((state) => state.auth.balance);

  useEffect(() => {
    const userData = localStorage.getItem("userData");
    if (userData) {
      JSON.parse(userData);
    } else {
      navigate("/login");
    }
    dispatch(getUpdatedBalance());
  }, []);

  const user = JSON.parse(localStorage.getItem("userData"));

  return (
    <div>
      <div className="my-5 flex items-center justify-center">
        <div className="bg-white p-8 rounded-lg shadow-md text-center">
          <h2 className="text-3xl font-semibold mb-4">
            Welcome To Your Account Page
          </h2>
          {user ? (
            <>
              <h4 className="text-xl mb-2">Account Name: {user.username}</h4>
              <h4 className="text-xl  mb-2">Account Number: {user.phoneNo}</h4>
              <h4 className="text-xl">
                Account Balance: {balance} dollars
              </h4>
            </>
          ) : (
            <Loading />
          )}
        </div>
      </div>
      <div>
        <TransactionCard />
      </div>
      <div>
        <TransactionHistory />
      </div>
    </div>
  );
};

export default MainPage;
