import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./features/auth/authSlice";
import transactionSlice from "./features/transaction/transaction.slice";

const store = configureStore({
  reducer: {
    auth: authReducer,
    trans: transactionSlice
  },
});

export default store;
