import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import client from "../../api/index";

const initialState = {
  transaction: null,
  loading: false,
  error: null,
};

export const depositeMoney = createAsyncThunk(
  "transaction/deposite",
  async (data, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("authToken");
      if (!accessToken) {
        return rejectWithValue("Access token not found.");
      }
      await client.post(
        "",
        {
          query: `
            mutation Deposit($amount: Float!) {
              deposit(amount: $amount) {
                amount
                timestamp
                type
                userId
              }
            }
          `,
          variables: {
            amount: data,
          },
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );
    } catch (error) {
      console.error("Deposite error:", error);
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const withDrawMoney = createAsyncThunk(
  "transaction/withDraw",
  async (data, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("authToken");
      if (!accessToken) {
        return rejectWithValue("Access token not found.");
      }
      await client.post(
        "",
        {
          query: `
            mutation Withdraw($amount: Float!) {
              withdraw(amount: $amount) {
                amount
                timestamp
                type
                userId
              }
            }
          `,
          variables: {
            amount: data,
          },
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );
    } catch (error) {
      console.error("WithDraw error:", error);

      return rejectWithValue(error.response.data.message);
    }
  }
);

export const transferMoney = createAsyncThunk(
  "transaction/transfer",
  async ({ amount, recipientPhoneNo }, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("authToken");
      if (!accessToken) {
        return rejectWithValue("Access token not found.");
      }
      await client.post(
        "",
        {
          query: `
          mutation transfer($amount: Float!, $recipientPhoneNo: String!) {
            transfer(amount: $amount, recipientPhoneNo: $recipientPhoneNo) {
              amount
              timestamp
              type
              userId
              recipientPhoneNo
            }
          }
          `,
          variables: {
            amount: amount,
            recipientPhoneNo: recipientPhoneNo
          },
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );
    } catch (error) {
      console.error("Transfer error:", error);
      console.log(error.response.data.message);
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const fetchTransactionHistory = createAsyncThunk(
  "transaction/fetchTransactionHistory",
  async (_, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("authToken");
      if (!accessToken) {
        return rejectWithValue("Access token not found.");
      }
      const response = await client.post(
        "",
        {
          query: `
            query TransactionHistory {
              transactionHistory {
                amount
                timestamp
                type
              }
            }
          `,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );

      return response.data.data.transactionHistory;
    } catch (error) {
      console.error("Fetch Transaction History error:", error);
      return rejectWithValue(error.response.data.message);
    }
  }
);

const transactionSlice = createSlice({
  name: "transaction",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(depositeMoney.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(depositeMoney.fulfilled, (state) => {
        state.loading = false;
      })
      .addCase(depositeMoney.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        if (state.error) {
          toast.error(action.error.message);
        }
      })
      .addCase(withDrawMoney.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(withDrawMoney.fulfilled, (state) => {
        state.loading = false;
      })
      .addCase(withDrawMoney.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        if (state.error) {
          toast.error(action.error.message);
        }
      })
      .addCase(transferMoney.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(transferMoney.fulfilled, (state) => {
        state.loading = false;
      })
      .addCase(transferMoney.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        if (state.error) {
          toast.error(action.error.message);
        }
      })
      .addCase(fetchTransactionHistory.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchTransactionHistory.fulfilled, (state, action) => {
        state.transaction = action.payload;
        state.loading = false;
      })
      .addCase(fetchTransactionHistory.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export default transactionSlice.reducer;
