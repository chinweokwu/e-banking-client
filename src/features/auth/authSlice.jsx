import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { toast } from "react-toastify";
import client from "../../api/index";

const initialState = {
  isAuthenticated: false,
  user: null,
  loading: false,
  error: null,
  balance: null,
};

export const registerUser = createAsyncThunk(
  "auth/registerUser",
  async (userData, { rejectWithValue }) => {
    try {
      await client.post("", {
        query: `
          mutation Signup($input: RegisterUserInput!) {
            signup(registerUserInput: $input) {
              username
              _id
              phoneNo
              balance
            }
          }
        `,
        variables: {
          input: userData,
        },
      });
    } catch (error) {
      // Handle errors more gracefully
      if (error.response) {
        return rejectWithValue(error.response.data.message);
      } else {
        return rejectWithValue("Registration failed: Unknown error");
      }
    }
  }
);

export const loginUser = createAsyncThunk(
  "auth/loginUser",
  async (userData, { rejectWithValue }) => {
    try {
      const response = await client.post("", {
        query: `
          mutation Login($input: LoginUserInput!) {
            login(loginUserInput: $input) {
              user {
                username
                phoneNo
                balance
                _id
              }
              access_token
            }
          }
        `,
        variables: {
          input: userData,
        },
      });

      const token = response.data.data.login.access_token;
      localStorage.setItem("authToken", token);
      localStorage.setItem("userData", JSON.stringify(response.data.data.login.user)); // Store user data


      return response.data.data.login.user;
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const getUpdatedBalance = createAsyncThunk(
  "user/getUpdatedBalance",
  async (_, { rejectWithValue }) => {
    try {
      const accessToken = localStorage.getItem("authToken");
      if (!accessToken) {
        return rejectWithValue("Access token not found.");
      }
      const response = await client.post(
        "",
        {
          query: `
          {
            myBalance
          }
          `,
        },
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        }
      );
      return response.data.data.myBalance;
    } catch (error) {
      return rejectWithValue(error.response.data.message);
    }
  }
);

export const logOut = () => ({
  type: "auth/logout",
});

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(registerUser.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(registerUser.fulfilled, (state) => {
        state.loading = false;
        state.isAuthenticated = true;
        if (state.isAuthenticated === true) {
          toast.success("User registered successfully");
        }
      })
      .addCase(registerUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        if (state.error) {
          toast.error(action.error.message);
        }
      })
      .addCase(loginUser.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(loginUser.fulfilled, (state, action) => {
        state.loading = false;
        state.user = action.payload;
        state.isAuthenticated = true;
        
        if (state.isAuthenticated === true) {
          toast.success("User logged in successfully");
        }
      })
      .addCase(loginUser.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
        if (state.error) {
          toast.error(action.error.message);
        }
      })
      .addCase(getUpdatedBalance.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(getUpdatedBalance.fulfilled, (state, action) => {
        state.balance = action.payload;
        state.loading = false;
      })
      .addCase(getUpdatedBalance.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
  },
});

export default authSlice.reducer;
