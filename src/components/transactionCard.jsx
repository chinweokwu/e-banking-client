import ModalForm from "./modal";
import { DollarOutlined, BulbOutlined, PoweroffOutlined } from "@ant-design/icons";
import { Form } from 'antd';
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { logOut } from "../features/auth/authSlice";
const TransactionCard = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogout = () => {
    // Dispatch the logout action to reset the user state
    dispatch(logOut());

    // Clear the localStorage token
    localStorage.removeItem("authToken");

    // Redirect to the login page
    navigate("/login");
  };
  return (
    <>
    <Form>
      <div className="flex flex-wrap justify-center mt-10">
        <div className="p-4 max-w-sm">
          <div className="flex rounded-lg h-full bg-teal-400 p-8 flex-col">
            <div className="flex items-center mb-3">
              <h2 className="text-white text-lg font-medium">
              <DollarOutlined style={{ fontSize: '32px', color: 'hotpink' }}/>
              </h2>
            </div>
            <div className="relative inline-flex group">
              <div className="absolute transition-all duration-1000 opacity-70 -inset-px bg-gradient-to-r from-[#44BCFF] via-[#FF44EC] to-[#FF675E] rounded-xl blur-lg group-hover:opacity-100 group-hover:-inset-1 group-hover:duration-200 animate-tilt"></div>
              <ModalForm action="Deposit" />
            </div>
          </div>
        </div>

        <div className="p-4 max-w-sm">
          <div className="flex rounded-lg h-full bg-teal-400 p-8 flex-col">
            <div className="flex items-center mb-3">
              <h2 className="text-white text-lg font-medium">
              <BulbOutlined style={{ fontSize: '32px', color: 'yellow' }} />
              </h2>
            </div>
            <div className="relative inline-flex group">
              <div className="absolute transition-all duration-1000 opacity-70 -inset-px bg-gradient-to-r from-[#44BCFF] via-[#FF44EC] to-[#FF675E] rounded-xl blur-lg group-hover:opacity-100 group-hover:-inset-1 group-hover:duration-200 animate-tilt"></div>
              <ModalForm action="Withdraw" />
            </div>
          </div>
        </div>

        <div className="p-4 max-w-sm">
          <div className="flex rounded-lg h-full bg-teal-400 p-8 flex-col">
            <div className="flex items-center mb-3">
              <h2 className="text-white text-lg font-medium">
              <BulbOutlined style={{ fontSize: '32px', color: 'yellow' }} />
              </h2>
            </div>
            <div className="relative inline-flex group">
              <div className="absolute transition-all duration-1000 opacity-70 -inset-px bg-gradient-to-r from-[#44BCFF] via-[#FF44EC] to-[#FF675E] rounded-xl blur-lg group-hover:opacity-100 group-hover:-inset-1 group-hover:duration-200 animate-tilt"></div>
              <ModalForm action="Transfer" />
            </div>
          </div>
        </div>

        <div className="p-4 max-w-sm">
          <div className="flex rounded-lg h-full bg-teal-400 p-8 flex-col">
            <div className="flex items-center mb-3">
              <h2 className="text-white text-lg font-medium">
              <PoweroffOutlined  style={{ fontSize: '32px', color: 'red' }}/>
              </h2>
            </div>
            <div className="relative inline-flex  group">
              <div className="absolute transitiona-all duration-1000 opacity-70 -inset-px bg-gradient-to-r from-[#44BCFF] via-[#FF44EC] to-[#FF675E] rounded-xl blur-lg group-hover:opacity-100 group-hover:-inset-1 group-hover:duration-200 animate-tilt"></div>
              <button
                onClick={handleLogout}
                className="relative inline-flex items-center justify-center px-4 py-4 text-sm font-bold text-white transition-all duration-200 bg-gray-900 font-pj rounded-xl focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-900"
              >
                Logut
              </button>
            </div>
          </div>
        </div>
      </div>
      </Form>

    </>
  );
};

export default TransactionCard;
