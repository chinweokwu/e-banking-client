import { useDispatch, useSelector } from "react-redux";
import { fetchTransactionHistory } from "../features/transaction/transaction.slice";
import { useEffect, useState } from "react";

const TransactionHistory = () => {
  const dispatch = useDispatch();
  const user = JSON.parse(localStorage.getItem("userData"));
  const [transactions, setTransactions] = useState([]);
  const loading = useSelector((state) => state.trans.loading);

  useEffect(() => {
    if (user && user._id) {
      // Dispatch the action to fetch transaction history for the logged-in user
      dispatch(fetchTransactionHistory())
        .unwrap()
        .then((data) => {
          setTransactions(data);
        });
    }
  }, [dispatch]);

  const formatTimestamp = (timestamp) => {
    const options = {
      year: "numeric",
      month: "short",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    };
    const formattedDate = new Intl.DateTimeFormat("en-US", options).format(
      new Date(timestamp)
    );

    const currentDate = new Date();
    const transactionDate = new Date(timestamp);
    const timeDifference = currentDate.getTime() - transactionDate.getTime();
    const daysAgo = Math.floor(timeDifference / (1000 * 60 * 60 * 24));

    if (daysAgo === 0) {
      return `${formattedDate}`;
    } else if (daysAgo === 1) {
      return `${formattedDate}`;
    } else {
      return `${daysAgo} days ago at ${formattedDate}`;
    }
  };


  return (
    <>
      <div className="container mx-auto px-4 sm:px-6 lg:px-8 py-8">
        <h2 className="text-2xl font-bold mb-4">Transaction History</h2>
        {loading ? (
          <p>Loading...</p>
        ) : (
          <table id="example" className="table-auto w-full">
            <thead>
              <tr>
                <th className="px-4 py-2">Amount(dollars)</th>
                <th className="px-4 py-2">Type(Withdraw/Deposit/Transfer)</th>
                <th className="px-4 py-2">Date/Time</th>
              </tr>
            </thead>
            <tbody>
              {transactions.length === 0 ? (
                <tr>
                  <td className="border px-4 py-2" colSpan="3">
                    No transactions found.
                  </td>
                </tr>
              ) : (
                transactions.map((transaction) => (
                  <tr key={transaction.timestamp}>
                    <td className="border px-4 py-2">{transaction.amount}</td>
                    <td className="border px-4 py-2">{transaction.type}</td>
                    <td className="border px-4 py-2">{formatTimestamp(transaction.timestamp)}</td>
                  </tr>
                ))
              )}
            </tbody>
          </table>
        )}
      </div>
    </>
  );
};

export default TransactionHistory;