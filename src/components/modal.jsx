/* eslint-disable react/prop-types */
import { useState } from 'react';
import { Button, Modal, Input, Form } from 'antd';
import { useDispatch } from 'react-redux';
import { withDrawMoney, depositeMoney, transferMoney } from '../features/transaction/transaction.slice';

const ModalForm = ({ action }) => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [form] = Form.useForm();

  const showModal = () => {
    setOpen(true);
  };

  const handleOk = async () => {
    try {
      const values = await form.validateFields();
      if (action === 'Deposit') {
        const amount = parseFloat(values.amount);
        dispatch(depositeMoney(amount));
      } else if (action === 'Withdraw') {
        const amount = parseFloat(values.amount);
        dispatch(withDrawMoney(amount));
      } else if (action === 'Transfer') {
        const amount = parseFloat(values.amount);
        const recipientPhoneNo = values.recipientPhoneNo;
        dispatch(transferMoney({ amount, recipientPhoneNo }));
      }
      form.resetFields();
      // window.location.reload();
    } catch (error) {
      console.error('Validation error:', error);
    }
  };

  const handleCancel = () => {
    form.resetFields();
    setOpen(false);
  };

  return (
    <Form.Provider>
      <Button type="primary" onClick={showModal}>
      {action === 'Deposit' ? 'Deposit' : action === 'Withdraw' ? 'Withdraw' : 'Transfer'}
      </Button>
      <Modal
        title={action === 'Deposit' ? 'Deposit' : action === 'Withdraw' ? 'Withdraw' : 'Transfer'}
        open={open}
        onOk={handleOk}
        onCancel={handleCancel}
        className="custom-modal"
      >
        <Form form={form} name={`modalForm-${action}`}>
          <Form.Item
            name="amount"
            label="Amount"
            rules={[
              {
                required: true,
                message: 'Please enter the amount!',
              },
            ]}
          >
            <Input type="number" />
          </Form.Item>
          {action === 'Transfer' && (
            <Form.Item
              name="recipientPhoneNo"
              label="Recipient's Phone Number"
              rules={[
                {
                  required: true,
                  message: "Please enter the recipient's phone number!",
                },
              ]}
            >
              <Input type="text" />
            </Form.Item>
          )}
        </Form>
      </Modal>
    </Form.Provider>
  );
};

export default ModalForm;